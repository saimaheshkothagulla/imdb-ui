let formEl = document.getElementById("myForm");
//movie name field
let nameFieldEl = document.getElementById("name");
let nameError = document.getElementById("nameError");
//year field
let yearFieldEl = document.getElementById("year");
let yearError = document.getElementById("yearError");

//profit/losss field
let plFieldEl = document.getElementById("pl");
let plError = document.getElementById("plError");

//plot field
let plotFieldEl = document.getElementById("plot");
let plotError = document.getElementById("plotError");

//poster field
let posterFieldEl = document.getElementById("poster");
let posterError = document.getElementById("posterError");

//producer filed
let producerFieldEl = document.getElementById("producer");
let producerError = document.getElementById("producerError");

//actors filed
let actorsFieldEl = document.getElementById("actors");
let actorsError = document.getElementById("actorsError");

//genres field

let genresFieldEl = document.getElementById("genres");
let genresError = document.getElementById("genresError");

let formValidationError = document.getElementById("formValidationError");

let movieDetails = {
    Name: "",
    YearOfRelease: "",
    Plot: "",
    ProducerId: "",
    Actors: "",
    Genres: "",
    Profit: "",
    Poster: ""
};

function formValidatation(formData) {
    let { Name, YearOfRelease, Plot, ProducerId, Actors, Genres, Profit, Poster } = formData;
    console.log("hello");
    if (Name === "") {
        nameError.textContent = "Required*";
    }
    if (YearOfRelease === "") {
        yearError.textContent = "Required*";
    }
    if (Profit === "") {
        plError.textContent = "Required*";
    }

    if (Plot === "") {
        plotError.textContent = "Required*";
    }
    if (Poster === "") {
        posterError.textContent = "Required*";

    }
    if (Actors === "") {
        actorsError.textContent = "Required*";

    }
    if (Genres === "") {
        genresError.textContent = "Required*";

    }
    if (ProducerId === 0 || ProducerId === "") {
        producerError.textContent = "Required *";

    }
    if (Name !== "" && YearOfRelease !== "" && (ProducerId !== 0 || ProducerId !== "") && Plot !== "" && Poster !== "" && Genres !== "" && Actors !== "") {
        //console.log("hello");
        return 1;
    } else {
        return 0;
    }
}

//removing default dehaviour
formEl.addEventListener("submit", function(event) {
    console.log(movieDetails);
    event.preventDefault();
    //form validation
    var validedOrNot = formValidatation(movieDetails);
    if (validedOrNot === 1) {
        console.log(validedOrNot);


        let body = movieDetails;
        let options = {
            method: "POST",
            headers: {
                "Content-Type": "application/json",
                Accept: "application/json"
            },
            body: JSON.stringify(body)


        };
        fetch("https://localhost:44330/movies", options).then(function(response) {
                return response.status;
            })
            .then(function(jsonData) {
                formValidationError.textContent = "submited successfully...";
                formValidationError.classList.add("successfull-submitaion");

                console.log(jsonData);
            });
    } else {
        formValidationError.textContent = "please enter all required fileds...";
        formValidationError.classList.remove("successfull-submitaion");
    }


});

//name field required
nameFieldEl.addEventListener("blur", function(event) {
    if (event.target.value === "") {
        nameError.textContent = "* Required";
        movieDetails.Name = "";
    } else {

        nameError.textContent = "";
        movieDetails.Name = event.target.value;
        // console.log(movieDetails);
    }
});


//year field required
yearFieldEl.addEventListener("blur", function(event) {
    if (event.target.value === "") {
        yearError.textContent = "* Required";
        movieDetails.YearOfRelease = "";
    } else {
        movieDetails.YearOfRelease = Number(event.target.value);
        yearError.textContent = "";
        // console.log(movieDetails);
    }
});

//profit or loss ammount required
plFieldEl.addEventListener("blur", function(event) {
    if (event.target.value === "") {
        plError.textContent = "* Required";
        movieDetails.Profit = "";
    } else {
        movieDetails.Profit = Number(event.target.value);
        plError.textContent = "";
        //console.log(movieDetails);
    }
});

//plot required
plotFieldEl.addEventListener("blur", function(event) {
    if (event.target.value === "") {
        plotError.textContent = "* Required";
        movieDetails.Plot = "";
    } else {
        movieDetails.Plot = event.target.value;
        plotError.textContent = "";
        //  console.log(movieDetails);
    }
});


//poster required
posterFieldEl.addEventListener("blur", function(event) {
    if (event.target.value === "") {
        posterError.textContent = "* Required";
        movieDetails.Poster = "";
    } else {
        movieDetails.Poster = event.target.value;
        posterError.textContent = "";
        //console.log(movieDetails);
    }
});


producerFieldEl.addEventListener("blur", function(event) {
    if (event.target.value === "") {
        producerError.textContent = "* Required";
        movieDetails.ProducerId = "";
    } else {
        movieDetails.ProducerId = Number(event.target.value);

        producerError.textContent = "";
        //console.log(movieDetails);
    }

});



actorsFieldEl.addEventListener("blur", function(event) {

    let actorsList = $("#actors").val().map(Number);


    if (actorsList.indexOf(0) === 0) {
        actorsList.shift();
        if (actorsList.length === 0) {
            actorsError.textContent = "* Required";
            movieDetails.Actors = "";
        }
    } else {

        movieDetails.Actors = actorsList;
        actorsError.textContent = "";
        //console.log(movieDetails);
    }



});


genresFieldEl.addEventListener("blur", function(event) {

    let genresList = $("#genres").val().map(Number);

    if (genresList.indexOf(0) === 0) {
        genresList.shift();
        if (genresList.length === 0) {
            genresError.textContent = "* Required";
            movieDetails.Genres = "";
        }
    } else {

        movieDetails.Genres = genresList;
        genresError.textContent = "";
        //console.log(movieDetails);
    }
});






//actors list
function getActors() {

    let options = {
        method: "GET",
        headers: {
            "Content-Type": "application/json",
            Accept: "application/json"
        }
    }

    fetch("https://localhost:44330/actors", options).then(function(response) {
            return response.json();
        })
        .then(function(jsonData) {

            localStorage.setItem("Actors", JSON.stringify(jsonData));


        });



}
getActors();

let tempActors = getActorListFromLocalStorage();

function getActorListFromLocalStorage() {
    let stringifiedActorList = localStorage.getItem("Actors");
    let parsedActorList = JSON.parse(stringifiedActorList);
    if (parsedActorList == null) {
        return [];
    } else {
        return parsedActorList;
    }
}



//actors adding function dynamically
function AddingActors(actor) {
    let option = document.createElement("option");
    option.value = actor.id;
    option.textContent = actor.name;
    actorsFieldEl.appendChild(option);


}
for (let actor of tempActors) {
    AddingActors(actor);
}


function getGenres() {

    let options = {
        method: "GET",
        headers: {
            "Content-Type": "application/json",
            Accept: "application/json"
        }
    }

    fetch("https://localhost:44330/genres", options).then(function(response) {
            return response.json();
        })
        .then(function(jsonData) {

            localStorage.setItem("Genres", JSON.stringify(jsonData));


        });



}
getGenres();
let tempGenres = getGenreListFromLocalStorage();

function getGenreListFromLocalStorage() {
    let stringifiedGenreList = localStorage.getItem("Genres");
    let parsedGenreList = JSON.parse(stringifiedGenreList);
    if (parsedGenreList == null) {
        return [];
    } else {
        return parsedGenreList;
    }
}
for (let genre of tempGenres) {
    AddingGenres(genre);
}

//genres adding function dynamically
function AddingGenres(genre) {
    let option = document.createElement("option");
    option.value = genre.id;
    option.textContent = genre.name;
    genresFieldEl.appendChild(option);

}




//GET :producers
function getProducers() {

    let options = {
        method: "GET",
        headers: {
            "Content-Type": "application/json",
            Accept: "application/json"
        }
    }

    fetch("https://localhost:44330/producers", options).then(function(response) {
            return response.json();
        })
        .then(function(jsonData) {

            localStorage.setItem("producers", JSON.stringify(jsonData));


        });



}
getProducers();

let tempProducers = getProducerListFromLocalStorage();

function getProducerListFromLocalStorage() {
    let stringifiedProducerList = localStorage.getItem("producers");
    let parsedProducerList = JSON.parse(stringifiedProducerList);
    if (parsedProducerList == null) {
        return [];
    } else {
        return parsedProducerList;
    }
}


for (let producer of tempProducers) {
    AddingProducers(producer);
}



//producers adding function dynamically
function AddingProducers(producer) {
    let option = document.createElement("option");
    option.value = producer.id;
    option.textContent = producer.name;
    producerFieldEl.appendChild(option);

}
let producerDetails = {
    Name: "",
    DOB: "",
    Bio: "",
    Gender: "Male"
};
let producerMaleGenderEl = document.getElementById("producerMaleGender");
producerMaleGenderEl.addEventListener("change", function(event) {
    producerDetails.Gender = event.target.value;
    console.log(producerDetails);
});
let producerFemaleGenderEl = document.getElementById("producerFemaleGender");
producerFemaleGenderEl.addEventListener("change", function(event) {
    producerDetails.Gender = event.target.value;
    console.log(producerDetails);
});

let producerNameFieldEl = document.getElementById("producerName");
producerNameFieldEl.addEventListener("change", function(event) {
    producerDetails.Name = event.target.value;


});
let producerDOBFieldEl = document.getElementById("producerDOB");
producerDOBFieldEl.addEventListener("change", function(event) {
    producerDetails.DOB = event.target.value;


});
let producerBioFieldEl = document.getElementById("producerBio");
producerBioFieldEl.addEventListener("change", function(event) {
    producerDetails.Bio = event.target.value;


});
let producerStatusMsg = document.getElementById("producerStatusMsg");
let producerFormEl = document.getElementById("producerForm");
producerFormEl.addEventListener("submit", function(event) {
    event.preventDefault();

    let { Name, DOB, Bio } = producerDetails;
    if (Name !== "" && DOB !== "" && Bio !== "") {

        let body = producerDetails;
        let options = {
            method: "POST",
            headers: {
                "Content-Type": "application/json",
                Accept: "application/json"
            },
            body: JSON.stringify(body)


        };
        fetch("https://localhost:44330/producers", options).then(function(response) {
                return response.status;
            })
            .then(function(jsonData) {
                producerStatusMsg.textContent = "submited successfully...";
                producerStatusMsg.classList.remove("error-msg");
                producerStatusMsg.classList.add("successfull-submitaion");
                console.log(jsonData);
                //updatePrdocuerSelection();


            });


    } else {
        producerStatusMsg.textContent = "Invalid details..";
    }


});



document.getElementById("producer").addEventListener("dblclick",
    function() {
        let x = document.getElementById("producer");
        for (let index = x.length - 1; index > 0; index--) {
            x.remove(index);

        }
        getProducers();
        let stringifiedProducerList = localStorage.getItem("producers");
        let parsedProducerList = JSON.parse(stringifiedProducerList);
        console.log(parsedProducerList);
        for (let producer of parsedProducerList) {
            AddingProducers(producer);
        }
    });



//actors

let actorDetails = {
    Name: "",
    DOB: "",
    Bio: "",
    Gender: "Male"
};
let actorMaleGenderEl = document.getElementById("actorMaleGender");
actorMaleGenderEl.addEventListener("change", function(event) {
    actorDetails.Gender = event.target.value;
    console.log(actorDetails);
});
let actorFemaleGenderEl = document.getElementById("actorFemaleGender");
actorFemaleGenderEl.addEventListener("change", function(event) {
    actorDetails.Gender = event.target.value;
    console.log(actorDetails);
});

let actorNameFieldEl = document.getElementById("actorName");
actorNameFieldEl.addEventListener("change", function(event) {
    actorDetails.Name = event.target.value;


});
let actorDOBFieldEl = document.getElementById("actorDOB");
actorDOBFieldEl.addEventListener("change", function(event) {
    actorDetails.DOB = event.target.value;


});
let actorBioFieldEl = document.getElementById("actorBio");
actorBioFieldEl.addEventListener("change", function(event) {
    actorDetails.Bio = event.target.value;


});
let actorStatusMsg = document.getElementById("actorStatusMsg");
let actorFormEl = document.getElementById("actorForm");
actorFormEl.addEventListener("submit", function(event) {
    event.preventDefault();

    let { Name, DOB, Bio } = actorDetails;
    if (Name !== "" && DOB !== "" && Bio !== "") {

        let body = actorDetails;
        let options = {
            method: "POST",
            headers: {
                "Content-Type": "application/json",
                Accept: "application/json"
            },
            body: JSON.stringify(body)


        };
        fetch("https://localhost:44330/actors", options).then(function(response) {
                return response.status;
            })
            .then(function(jsonData) {
                actorStatusMsg.textContent = "submited successfully...";
                actorStatusMsg.classList.remove("error-msg");
                actorStatusMsg.classList.add("successfull-submitaion");
                console.log(jsonData);
                // updatePrdocuerSelection();


            });


    } else {
        actorStatusMsg.textContent = "Invalid details..";
    }


});

document.getElementById("actorsEvent").addEventListener("dblclick", function() {
    var x = document.getElementById("actors");
    console.log(x);


    for (let index = x.length - 1; index > 0; index--) {
        x.remove(index);

    }
    getActors();
    let stringifiedActorList = localStorage.getItem("Actors");
    let parsedActorList = JSON.parse(stringifiedActorList);
    console.log(parsedActorList);
    for (let actor of parsedActorList) {
        AddingActors(actor);
    }

});