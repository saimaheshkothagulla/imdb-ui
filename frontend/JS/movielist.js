let allMoviesList = document.getElementById("allMovies");
let editFormEl = document.getElementById("myEditForm");
let validationKey = 0;

// let movies = [{
//         id: 1,
//         name: "dookudu",
//         yearofRelease: 2019,
//         plot: "hi hello how r u",
//         actors: [1, 2],
//         producerId: 2,
//         poster: "srimanthudu.jpg",
//         genres: [1, 2]
//     },
//     {
//         id: 2,
//         name: "dookudu 1",
//         yearofRelease: 2014,
//         plot: "hi hello how r u",
//         actors: [1, 2],
//         producerId: 1,
//         poster: "srimanthudu.jpg",
//         genres: [1, 2]

//     }
// ];


// let actors = [{
//         id: 1,
//         name: "maheshbabu"
//     },
//     {
//         id: 2,
//         name: "samnatha"
//     }
// ];

// let producers = [

//     {
//         id: 1,
//         name: "dil raju"
//     },
//     {
//         id: 2,
//         name: "suresh"
//     }
// ];
// let genres = [

//     {
//         id: 1,
//         name: "comdey"
//     },
//     {
//         id: 2,
//         name: "action"
//     }
// ];



let tempId = 0;





function onDeleteMovie(containerId) {
    let movie = document.getElementById(containerId);
    allMoviesList.removeChild(movie);
    let deleteId = 0;
    let deleteMovieIndex = movies.findIndex(function(eachMovie) {
        let eachMovieId = "container" + eachMovie.id;
        if (eachMovieId === containerId) {
            deleteId = eachMovie.id;
            return true;
        } else {
            return false;
        }
    });
    let options = {
        method: "DELETE",
        headers: {
            "Content-Type": "application/json",
            Accept: "application/json"
        }
    }
    let url = "https://localhost:44330/movies/" + deleteId;
    fetch(url, options).then(function(response) {
            return response.json();
        })
        .then(function(jsonData) {

            getMovies();

        });





    movies.splice(deleteMovieIndex, 1);
    //console.log(movies);
}
getMovies();

function getMovies() {

    let options = {
        method: "GET",
        headers: {
            "Content-Type": "application/json",
            Accept: "application/json"
        }
    }

    fetch("https://localhost:44330/movies", options).then(function(response) {
            return response.json();
        })
        .then(function(jsonData) {

            localStorage.setItem("movies", JSON.stringify(jsonData));

        });

}
let movies = getMoviesListFromLocalStorage();

function getMoviesListFromLocalStorage() {
    let stringifiedMoviesList = localStorage.getItem("movies");
    let parsedMoviesList = JSON.parse(stringifiedMoviesList);
    if (parsedMoviesList == null) {
        return [];
    } else {
        return parsedMoviesList;
    }
}
for (let movie of movies) {
    AddMovieCard(movie);
}


function AddMovieCard(movie) {
    let containerId = "container" + movie.id;
    let movieId = "movie" + movie.id;
    let container = document.createElement("div");
    container.id = containerId;
    container.classList.add("col-12", "col-md-4", "col-lg-3");
    allMoviesList.appendChild(container);

    let movieCard = document.createElement("div");
    movieCard.id = "movieCard" + movieId;
    movieCard.classList.add("movie-card", "shadow");
    container.appendChild(movieCard);


    //image -div
    let imageContainer = document.createElement("div");
    imageContainer.classList.add("card-img", "h-35", "w-100");
    movieCard.appendChild(imageContainer);

    //poster
    let poster = document.createElement("img");
    poster.setAttribute("src", movie.poster);
    poster.classList.add("poster");
    imageContainer.appendChild(poster);

    //card-body
    let cardBody = document.createElement("div");
    cardBody.classList.add("movie-body", "p-4");
    movieCard.appendChild(cardBody);
    //moviename
    let movieName = document.createElement("h4");
    movieName.id = "movieName" + movieId;
    console.log(movie);
    movieName.textContent = movie.name + "(" + movie.yearOfRelease + ")";
    movieName.classList.add("name")
    cardBody.appendChild(movieName);

    //movie-details
    let movieDetails = document.createElement("p");
    movieDetails.id = "movieDetails" + movieId;
    movieDetails.textContent = movie.plot;
    movieDetails.classList.add("mb-1", "plot");
    cardBody.appendChild(movieDetails);

    let producerContainer = document.createElement("div");
    cardBody.appendChild(producerContainer);

    let producerlabel = document.createElement("label");
    producerlabel.textContent = "Producer: "
    producerContainer.appendChild(producerlabel);



    let producerName = document.createElement("h6");
    producerName.classList.add("d-inline", "producer-name");
    producerName.textContent = movie.producer.name;
    producerContainer.appendChild(producerName);



    let actorgenreContainer = document.createElement("div");
    actorgenreContainer.classList.add("d-flex", "justify-content-between");
    cardBody.appendChild(actorgenreContainer);

    //list of actors;
    let actorsContainer = document.createElement("div");
    actorsContainer.classList.add("list-actors");
    actorgenreContainer.appendChild(actorsContainer);

    //actor heading
    let actorHead = document.createElement("h5");
    actorHead.classList.add("d-inline");
    actorHead.textContent = "Actors";
    actorsContainer.appendChild(actorHead);

    //actors
    let unOrderedActorList = document.createElement("ul");
    unOrderedActorList.classList.add("p-1");
    actorsContainer.appendChild(unOrderedActorList);

    //list
    movie.actors.forEach(actor => {


            let li = document.createElement("li");
            li.textContent = actor.name;
            unOrderedActorList.appendChild(li);
        }

    );

    //list of genress;
    let genresContainer = document.createElement("div");
    actorgenreContainer.appendChild(genresContainer);

    //genreheading
    let genreHead = document.createElement("h5");
    genreHead.classList.add("d-inline");
    genreHead.textContent = "Genres";
    genresContainer.appendChild(genreHead);

    //genres
    let unOrderedgenreList = document.createElement("ul");
    unOrderedgenreList.classList.add("p-1");
    genresContainer.appendChild(unOrderedgenreList);

    //list
    movie.genres.forEach(genre => {


        let li = document.createElement("li");
        li.textContent = genre.name;
        unOrderedgenreList.appendChild(li);

    });



    //card edit and delete
    let cardModifyContainer = document.createElement("div");
    cardModifyContainer.classList.add("d-flex", "justify-content-between");
    cardBody.appendChild(cardModifyContainer);
    //edit 
    let editLabel = document.createElement("label");
    editLabel.classList.add("text-primary");
    cardModifyContainer.appendChild(editLabel);

    // let atag = document.createElement("a");
    // atag.href = "editmovie.html";
    // editLabel.appendChild(atag);

    editIcon = document.createElement("i");
    editIcon.id = "editIcon" + movie.id;
    //data-toggle="modal" data-target="#myModal"
    editIcon.setAttribute("data-toggle", "modal");
    editIcon.setAttribute("data-target", "#myModal");

    editIcon.classList.add("far", "fa-edit", "icon");
    editIcon.onclick = function() {
        tempId = movie.id;
        nameError.textContent = "";
        yearError.textContent = "";
        plError.textContent = "";
        posterError.textContent = "";
        plotError.textContent = "";
        actorsError.textContent = "";
        producerError.textContent = "";
        genresError.textContent = "";
        formValidationError.textContent = "";
    }
    editLabel.appendChild(editIcon);
    //delete

    let deleteLabel = document.createElement("label");
    deleteLabel.classList.add("text-danger");
    cardModifyContainer.appendChild(deleteLabel);

    deleteIcon = document.createElement("i");
    deleteIcon.id = "deleteIcon" + movie.id;
    deleteIcon.classList.add("far", "fa-trash-alt", "icon", "deleteIcon");
    deleteIcon.setAttribute("data-toggle", "modal");
    deleteIcon.setAttribute("data-target", "#deleteModal");
    deleteIcon.onclick = function() {

        onDeletePopUp(movie.name, containerId);

    }
    deleteLabel.appendChild(deleteIcon);

}

function onDeletePopUp(name, containerId) {
    document.getElementById("popUpName").textContent = name;
    let deleteButton = document.getElementById("popUpDeleteButton");
    deleteButton.onclick = function() {
        onDeleteMovie(containerId);
        deleteButton.setAttribute("data-dismiss", "modal");
    }

}






let nameFieldEl = document.getElementById("name");
let nameError = document.getElementById("nameError");
//year field
let yearFieldEl = document.getElementById("year");
let yearError = document.getElementById("yearError");

//profit/losss field
let plFieldEl = document.getElementById("pl");
let plError = document.getElementById("plError");

//plot field
let plotFieldEl = document.getElementById("plot");
let plotError = document.getElementById("plotError");

//poster field
let posterFieldEl = document.getElementById("poster");
let posterError = document.getElementById("posterError");

//producer filed
let producerFieldEl = document.getElementById("producer");
let producerError = document.getElementById("producerError");

//actors filed
let actorsFieldEl = document.getElementById("actors");
let actorsError = document.getElementById("actorsError");

//genres field

let genresFieldEl = document.getElementById("genres");
let genresError = document.getElementById("genresError");

let formValidationError = document.getElementById("formValidationError");

let movieDetails = {
    Name: "",
    YearOfRelease: "",
    Plot: "",
    ProducerId: "",
    Actors: "",
    Genres: "",
    Profit: "",
    Poster: ""
};

function formValidatation(formData) {
    let { Name, YearOfRelease, Plot, ProducerId, Actors, Genres, Profit, Poster } = formData;
    console.log("hello");
    if (Name === "") {
        nameError.textContent = "Required*";
    }
    if (YearOfRelease === "") {
        yearError.textContent = "Required*";
    }
    if (Profit === "") {
        plError.textContent = "Required*";
    }

    if (Plot === "") {
        plotError.textContent = "Required*";
    }
    if (Poster === "") {
        posterError.textContent = "Required*";

    }
    if (Actors === "") {
        actorsError.textContent = "Required*";

    }
    if (Genres === "") {
        genresError.textContent = "Required*";

    }
    if (ProducerId === 0 || ProducerId === "") {
        producerError.textContent = "Required *";

    }
    if (Name !== "" && YearOfRelease !== "" && (ProducerId !== 0 || ProducerId !== "") && Plot !== "" && Poster !== "" && Genres !== "" && Actors !== "") {
        //console.log("hello");
        validationKey = 1;
        localStorage.setItem("validationKey", JSON.stringify(validationKey));

        console.log(validationKey);
        return 1;
    } else {
        localStorage.setItem("validationKey", JSON.stringify(validationKey));
        console.log(validationKey);
        return 0;
    }
}

//removing default dehaviour
editFormEl.addEventListener("submit", function(event) {
    event.preventDefault();
    //form validation
    var validedOrNot = formValidatation(movieDetails);
    if (validedOrNot === 1) {
        console.log(validedOrNot);


        let body = movieDetails;
        console.log(movieDetails);
        let options = {
            method: "PUT",
            headers: {
                "Content-Type": "application/json",
                Accept: "application/json"
            },
            body: JSON.stringify(body)


        };
        let urlPost = "https://localhost:44330/movies/" + tempId;
        fetch(urlPost, options).then(function(response) {
                return response.status;
            })
            .then(function(status) {
                formValidationError.textContent = "submited successfully...";
                //data-dismiss="modal"
                document.getElementById("submitButton").setAttribute("data-dismiss", "modal");

                formValidationError.classList.add("successfull-submitaion");
                let uniqueId = setTimeout(function() {
                    window.location = "/imdb/HTML/moviesList.html";
                }, 2000);

            });
    } else {
        formValidationError.textContent = "please enter all required fileds...";
        formValidationError.classList.remove("successfull-submitaion");
    }


});

//name field required
nameFieldEl.addEventListener("blur", function(event) {
    if (event.target.value === "") {
        nameError.textContent = "* Required";
        movieDetails.Name = "";
    } else {

        nameError.textContent = "";
        movieDetails.Name = event.target.value;
        // console.log(movieDetails);
    }
});


//year field required
yearFieldEl.addEventListener("blur", function(event) {
    if (event.target.value === "") {
        yearError.textContent = "* Required";
        movieDetails.YearOfRelease = "";
    } else {
        movieDetails.YearOfRelease = Number(event.target.value);
        yearError.textContent = "";
        // console.log(movieDetails);
    }
});

//profit or loss ammount required
plFieldEl.addEventListener("blur", function(event) {
    if (event.target.value === "") {
        plError.textContent = "* Required";
        movieDetails.Profit = "";
    } else {
        movieDetails.Profit = Number(event.target.value);
        plError.textContent = "";
        //console.log(movieDetails);
    }
});

//plot required
plotFieldEl.addEventListener("blur", function(event) {
    if (event.target.value === "") {
        plotError.textContent = "* Required";
        movieDetails.Plot = "";
    } else {
        movieDetails.Plot = event.target.value;
        plotError.textContent = "";
        //  console.log(movieDetails);
    }
});


//poster required
posterFieldEl.addEventListener("blur", function(event) {
    if (event.target.value === "") {
        posterError.textContent = "* Required";
        movieDetails.Poster = "";
    } else {
        movieDetails.Poster = event.target.value;
        posterError.textContent = "";
        //console.log(movieDetails);
    }
});







producerFieldEl.addEventListener("blur", function(event) {
    if (event.target.value === "") {
        producerError.textContent = "* Required";
        movieDetails.ProducerId = "";
    } else {
        movieDetails.ProducerId = Number(event.target.value);
        producerError.textContent = "";
        //console.log(movieDetails);
    }

});



actorsFieldEl.addEventListener("blur", function(event) {

    let actorsList = $("#actors").val().map(Number);

    if (actorsList.indexOf(0) === 0) {
        actorsList.shift();
        if (actorsList.length === 0) {
            actorsError.textContent = "* Required";
            movieDetails.Actors = "";
        }
    } else {

        movieDetails.Actors = actorsList;
        actorsError.textContent = "";
        //console.log(movieDetails);
    }



});


genresFieldEl.addEventListener("blur", function(event) {

    let genresList = $("#genres").val().map(Number);

    if (genresList.indexOf(0) === 0) {
        genresList.shift();
        if (genresList.length === 0) {
            genresError.textContent = "* Required";
            movieDetails.Genres = "";
        }
    } else {

        movieDetails.Genres = genresList;
        genresError.textContent = "";
        //console.log(movieDetails);
    }
});






//actors list
function getActors() {

    let options = {
        method: "GET",
        headers: {
            "Content-Type": "application/json",
            Accept: "application/json"
        }
    }

    fetch("https://localhost:44330/actors", options).then(function(response) {
            return response.json();
        })
        .then(function(jsonData) {

            localStorage.setItem("Actors", JSON.stringify(jsonData));


        });



}
getActors();

let tempActors = getActorListFromLocalStorage();

function getActorListFromLocalStorage() {
    let stringifiedActorList = localStorage.getItem("Actors");
    let parsedActorList = JSON.parse(stringifiedActorList);
    if (parsedActorList == null) {
        return [];
    } else {
        return parsedActorList;
    }
}
for (let actor of tempActors) {
    AddingActors(actor);
}



//actors adding function dynamically
function AddingActors(actor) {
    let option = document.createElement("option");
    option.value = actor.id;
    option.textContent = actor.name;
    actorsFieldEl.appendChild(option);

}


function getGenres() {

    let options = {
        method: "GET",
        headers: {
            "Content-Type": "application/json",
            Accept: "application/json"
        }
    }

    fetch("https://localhost:44330/genres", options).then(function(response) {
            return response.json();
        })
        .then(function(jsonData) {

            localStorage.setItem("Genres", JSON.stringify(jsonData));


        });



}
getGenres();
let tempGenres = getGenreListFromLocalStorage();

function getGenreListFromLocalStorage() {
    let stringifiedGenreList = localStorage.getItem("Genres");
    let parsedGenreList = JSON.parse(stringifiedGenreList);
    if (parsedGenreList == null) {
        return [];
    } else {
        return parsedGenreList;
    }
}
for (let genre of tempGenres) {
    AddingGenres(genre);
}

//genres adding function dynamically
function AddingGenres(genre) {
    let option = document.createElement("option");
    option.value = genre.id;
    option.textContent = genre.name;
    genresFieldEl.appendChild(option);

}






//GET :producers
function getProducers() {
    var producers = [];
    let options = {
        method: "GET",
        headers: {
            "Content-Type": "application/json",
            Accept: "application/json"
        }
    }

    fetch("https://localhost:44330/producers", options).then(function(response) {
            return response.json();
        })
        .then(function(jsonData) {

            localStorage.setItem("producers", JSON.stringify(jsonData));


        });



}
getProducers();
let tempProducers = getProducerListFromLocalStorage();

function getProducerListFromLocalStorage() {
    let stringifiedProducerList = localStorage.getItem("producers");
    let parsedProducerList = JSON.parse(stringifiedProducerList);
    if (parsedProducerList == null) {
        return [];
    } else {
        return parsedProducerList;
    }
}
for (let producer of tempProducers) {
    AddingProducers(producer);
}


//producers adding function dynamically
function AddingProducers(producer) {
    //console.log(producer.id)
    let option = document.createElement("option");
    option.value = producer.id;
    console.log(producer.name);
    option.textContent = producer.name;
    producerFieldEl.appendChild(option);

}