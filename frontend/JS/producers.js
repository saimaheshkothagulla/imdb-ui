let producersContainer = document.getElementById("listOfPersons");

let producers = JSON.parse(localStorage.getItem("producers"));
console.log(producers);


function onAddProducer(producer) {
    let card = document.createElement("div");
    card.id = "actor" + producer.id;
    //col-12 col-md-6 col-lg-4
    card.classList.add("each-card", "col-12", "col-md-6", "col-lg-4", "mb-3");
    producersContainer.appendChild(card);

    cardInfo = document.createElement("div");
    cardInfo.classList.add("card", "p-3", "shadow");
    card.appendChild(cardInfo);


    let cardDetails = document.createElement("div");
    cardDetails.classList.add("card-body-details");
    cardInfo.appendChild(cardDetails);

    let producerName = document.createElement("h4");
    producerName.classList.add("person-name");
    producerName.textContent = producer.name;
    cardDetails.appendChild(producerName);

    let gender = document.createElement("h6")
    gender.textContent = "Gender: " + producer.gender;
    cardDetails.appendChild(gender);

    let age = document.createElement("label");
    let dob = new Date(producer.dob).toDateString("MM/dd/yyyy");
    age.textContent = "age: " + dob;
    cardDetails.appendChild(age);

}

producers.forEach(producer => {
    onAddProducer(producer);

});