let actorsContainer = document.getElementById("listOfPersons");

let actors = JSON.parse(localStorage.getItem("Actors"));

console.log(actors); {
    /* <div class="row" id="listOfPersons">
    <div class="shadow each-card col-12 col-md-6 col-lg-4">
        <div class="card p-3">

            <div class="card-body-details">
                <h4 class="person-name">MaheshBabu</h4>
                <h6 class="person-gender">Gender: </h6>
                <label class="person-age">Age: </label>
            </div>
        </div>
    </div>

    </div> */
}

function onAddActor(actor) {
    let card = document.createElement("div");
    card.id = "actor" + actor.id;
    //col-12 col-md-6 col-lg-4
    card.classList.add("each-card", "col-12", "col-md-6", "col-lg-4", "mb-3");
    actorsContainer.appendChild(card);

    cardInfo = document.createElement("div");
    cardInfo.classList.add("card", "p-3", "shadow");
    card.appendChild(cardInfo);


    let cardDetails = document.createElement("div");
    cardDetails.classList.add("card-body-details");
    cardInfo.appendChild(cardDetails);

    let actorName = document.createElement("h4");
    actorName.classList.add("person-name");
    actorName.textContent = actor.name;
    cardDetails.appendChild(actorName);

    let gender = document.createElement("h6")
    gender.textContent = "Gender: " + actor.gender;
    cardDetails.appendChild(gender);

    let age = document.createElement("label");
    let dob = new Date(actor.dob).toDateString("MM/dd/yyyy");
    age.textContent = "age: " + dob;
    cardDetails.appendChild(age);

}

actors.forEach(actor => {
    onAddActor(actor);

});